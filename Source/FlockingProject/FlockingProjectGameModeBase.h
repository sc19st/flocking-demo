// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FlockingProjectGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class FLOCKINGPROJECT_API AFlockingProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
