// Fill out your copyright notice in the Description page of Project Settings.


#include "FlockManager.h"
#include "FlockActorInstance.h"

FVector GenerateSpawnFromRange(const FVector Min, const FVector Max)
{
	FVector Final;
	Final.X = FMath::RandRange(Min.X, Max.X);
	Final.Y = FMath::RandRange(Min.Y, Max.Y);
	Final.Z = FMath::RandRange(Min.Z, Max.Z);

	return Final;
}

// Sets default values
AFlockManager::AFlockManager()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MinRegionSize = FVector(-2500, -2500, -2500);
	MaxRegionSize = FVector(2500, 2500, 2500);

	RadiusOfInfluence = 1250.0f;

	SeperationPower = 1.0f;
	CohesionPower = 1.0f;
	AlignmentPower = 1.0f;

	BoidCount = 300;
}

//Spawn Flock Instance
AFlockActorInstance* AFlockManager::SpawnFlockInstance(const FVector Location, const FRotator Rotation)
{
	FActorSpawnParameters Params;
	Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	if(FlockInstanceClass != nullptr)
	{
		AFlockActorInstance* Instance = GetWorld()->SpawnActor<AFlockActorInstance>(FlockInstanceClass, Location, Rotation, Params);
		return Instance;
	}
	
	return nullptr;
}

void AFlockManager::InitialiseFlock()
{
	ClearFlock();
	for (int i = 0; i < BoidCount; i++)
	{
		FVector SpawnLocation = GenerateSpawnFromRange(MinRegionSize, MaxRegionSize);
		AFlockActorInstance* Instance = SpawnFlockInstance(SpawnLocation, FRotator(0, 0, 0));
		if(Instance)
		{
			Instance->SetFlockManager(this);
			Instance->SetPosition(SpawnLocation);
		}
		

		Instances.Add(Instance);
	}
}

void AFlockManager::ClearFlock()
{
	for (auto Instance : Instances)
		Instance->Destroy();
	
	Instances.Empty();
}

void AFlockManager::UpdateFlock(float DeltaTime)
{
	for (auto Instance : Instances)
	{
		Instance->Flock(Instances);
		Instance->Update(DeltaTime);
	}
}

//
void AFlockManager::BeginPlay()
{
	Super::BeginPlay();
	InitialiseFlock();
}

// Called every frame
void AFlockManager::Tick(float DeltaTime)
{
	UpdateFlock(DeltaTime);
	Super::Tick(DeltaTime);
}

float AFlockManager::GetSeperationPower()
{
	return SeperationPower;
}

float AFlockManager::GetCohesionPower()
{
	return CohesionPower;
}

float AFlockManager::GetAlignmentPower()
{
	return AlignmentPower;
}

