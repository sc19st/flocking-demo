// Fill out your copyright notice in the Description page of Project Settings.


#include "FlockActorInstance.h"

#include "DrawDebugHelpers.h"
#include "FlockManager.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"

#define COLLISION_OBSTACLE ECC_GameTraceChannel1

//Global variable caching the fibonacci sphere
TArray<FVector> CachedPositions;

TArray<FVector> GetTraceSphere(int NumTraces, float TraceDistance)
{
	TArray<FVector> TracePositions;

	//Create FIBONACCI SPHERE
	//Calculate PHI using the golden ration
	float phi = 2 * PI * UE_GOLDEN_RATIO;
		
	for(int i = 0; i < NumTraces; i++)
	{
		float Distance = static_cast<float>(i) / static_cast<float>(NumTraces);
		float LatitudeAngle = FMath::Acos(1 - 2 * Distance);
		float LongitudeAngle = i * phi;

		FVector Position(0,0,0);
		Position.X = TraceDistance * FMath::Sin(LatitudeAngle) * FMath::Cos(LongitudeAngle);
		Position.Y = TraceDistance * FMath::Sin(LatitudeAngle) * FMath::Sin(LongitudeAngle);
		Position.Z = TraceDistance * FMath::Cos(LatitudeAngle);
		
		TracePositions.Add(Position);
	}
	
	return TracePositions;
}

// Sets default values
AFlockActorInstance::AFlockActorInstance()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	FlockManager = nullptr;
	
	MaxSpeed = 700.0f;
	MaxForce = 500.0f;
	AccelerationMult = 50.0f;
	MaxAvoidanceForce = 200.0f;
	
	AvoidObstacles = true;

	//Find Mesh Asset
	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Game/Meshes/Bird.Bird'"));

	//Initialise Mesh
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);
	Mesh->SetStaticMesh(MeshAsset.Object);
	Mesh->SetGenerateOverlapEvents(false);
	Mesh->SetCollisionEnabled( ECollisionEnabled::QueryOnly);
	Mesh->SetCollisionResponseToChannel(COLLISION_OBSTACLE, ECollisionResponse::ECR_Ignore);
	
	//Set Initial motion values
	Position = FVector(0, 0, 0);
	Velocity = FVector(0,0,0);
	//Velocity = FVector(FMath::RandRange(-50, 50), FMath::RandRange(-50, 50), FMath::RandRange(-50, 50)).GetUnsafeNormal() * MaxSpeed;
	Acceleration = FVector(0, 0, 0);
}

void AFlockActorInstance::Update(float DeltaTime)
{
	//Update motion
	Position = Position + Velocity * DeltaTime;
	Velocity = Velocity + Acceleration * DeltaTime;
	Velocity = Velocity.GetClampedToMaxSize(MaxSpeed);

	//Rotate towards Velocity Direction
	FRotator DesiredRotation = UKismetMathLibrary::FindLookAtRotation(Position, Position + Velocity);
	SetActorRotation(DesiredRotation);
	
	//DetectEdge();
	UpdatePosition();
}

void AFlockActorInstance::DetectEdge()
{
	FVector MinRegionSize = FlockManager->MinRegionSize;
	FVector MaxRegionSize = FlockManager->MaxRegionSize;

	if (Position.X > MaxRegionSize.X)
		Position.X = MinRegionSize.X;
	else if(Position.X < MinRegionSize.X)
		Position.X = MaxRegionSize.X;

	if (Position.Y > MaxRegionSize.Y)
		Position.Y = MinRegionSize.Y;
	else if (Position.Y < MinRegionSize.Y)
		Position.Y = MaxRegionSize.Y;

	if (Position.Z > MaxRegionSize.Z)
		Position.Z = MinRegionSize.Z;
	else if (Position.Z < MinRegionSize.Z)
		Position.Z = MaxRegionSize.Z;
}

FVector AFlockActorInstance::CheckObstacles()
{
	if(CachedPositions.Num() == 0)
		CachedPositions.Append(GetTraceSphere(15, 330.0f));

	FVector AccAvoidanceForce(0,0,0);
	
	FCollisionQueryParams QueryParams(FName(TEXT("ObstacleTrace")), false, this);
	QueryParams.bTraceComplex = false;

	for(auto UnitTracePos : CachedPositions)
	{
		//DrawDebugLine(GetWorld(), Position, Position + UnitTracePos, FColor::Red, false, 0.5f);
		FVector EndPos = Position + UnitTracePos;
		FHitResult Result;
		bool SuccessHit = GetWorld()->LineTraceSingleByChannel(Result, Position, EndPos, COLLISION_OBSTACLE, QueryParams, FCollisionResponseParams::DefaultResponseParam);
		if(SuccessHit && Result.bBlockingHit)
		{
			
			FVector TraceDir = EndPos - Position;
			FVector ActorCenter, ActorBounds;
			Result.Actor->GetActorBounds(true, ActorCenter, ActorBounds, false);
			//ActorCenter = Result.ImpactPoint;
			
			FVector AvoidanceDir = (TraceDir - ActorCenter);
			AccAvoidanceForce += ( AvoidanceDir.GetSafeNormal() * MaxAvoidanceForce * FMath::Abs( FVector::DotProduct(Velocity.GetSafeNormal(), TraceDir.GetSafeNormal()) ) );
		}
	}
	
	return AccAvoidanceForce * 10.0f;
}

void AFlockActorInstance::Flock(TArray<AFlockActorInstance*> Neighbours)
{
	float SphereOfInfluence = FlockManager->RadiusOfInfluence;
	
	//Information to calculate Alignment
	FVector AlignVelocity(0, 0, 0);

	FVector CohesionVelocity(0,0,0);
	FVector AveragePosition(0, 0, 0);

	FVector SeperationVelocity(0,0,0);
	FVector AverageDirection(0, 0, 0);

	float total = 0.0f;

	for (auto Instance : Neighbours)
	{
		if (Instance != this && GetDistanceTo(Instance) <= SphereOfInfluence)
		{
			//Get Information for alignment
			AlignVelocity += Instance->GetCurrentVelocity();

			//Get Information for Cohesion
			AveragePosition += Instance->GetPosition();

			//Get Information for Seperation
			AverageDirection += ( ( Position - Instance->GetPosition() ) / FMath::Pow(GetDistanceTo(Instance), 2) );
			
			//Count Total
			total += 1.0f;
		}
	}

	if (total > 0.0f)
	{
		AlignVelocity /= total;
		AlignVelocity = AlignVelocity.GetUnsafeNormal() * MaxSpeed;
		AlignVelocity -= Velocity;
		AlignVelocity = AlignVelocity.GetClampedToMaxSize(MaxForce);

		AveragePosition /= total;
		CohesionVelocity = AveragePosition - Position;
		CohesionVelocity = (CohesionVelocity / 4) * MaxSpeed;
		CohesionVelocity -= Velocity;
		CohesionVelocity = CohesionVelocity.GetClampedToMaxSize(MaxForce);

		AverageDirection /= total;
		SeperationVelocity = AverageDirection.GetUnsafeNormal() * MaxSpeed;
		SeperationVelocity -= Velocity;
		SeperationVelocity = SeperationVelocity.GetClampedToMaxSize(MaxForce);
	}

	//If obstacle avoidance is enabled then check for obstacles
	FVector AvoidanceDir(0,0,0);
	if(AvoidObstacles)
		AvoidanceDir = CheckObstacles();
	
	AlignVelocity *= (FlockManager->GetAlignmentPower());
	CohesionVelocity *= (FlockManager->GetCohesionPower());
	SeperationVelocity *= (FlockManager->GetSeperationPower());

	FVector FinalAcceleration = SeperationVelocity + AlignVelocity + CohesionVelocity + AvoidanceDir;
	//FinalAcceleration *= AccelerationMult;
	Acceleration = FinalAcceleration;
}

// Called when the game starts or when spawned
void AFlockActorInstance::BeginPlay()
{
	Super::BeginPlay();
	Velocity = FVector(FMath::RandRange(-50, 50), FMath::RandRange(-50, 50), FMath::RandRange(-50, 50)).GetUnsafeNormal() * MaxSpeed;
	//Velocity = FVector(0,0,-1) * MaxSpeed;
}

// Called every frame
void AFlockActorInstance::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFlockActorInstance::SetPosition(FVector inPosition)
{
	Position = inPosition;
}

void AFlockActorInstance::UpdatePosition()
{
	SetActorLocation(Position);
}

FVector AFlockActorInstance::GetPosition()
{
	return Position;
}

FVector AFlockActorInstance::GetCurrentVelocity()
{
	return Velocity;
}

FVector AFlockActorInstance::GetAcceleration()
{
	return Acceleration;
}

void AFlockActorInstance::SetFlockManager(AFlockManager* Manager)
{
	FlockManager = Manager;
}

