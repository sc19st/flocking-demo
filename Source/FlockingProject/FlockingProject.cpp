// Copyright Epic Games, Inc. All Rights Reserved.

#include "FlockingProject.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, FlockingProject, "FlockingProject" );
