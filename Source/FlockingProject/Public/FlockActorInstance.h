// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FlockActorInstance.generated.h"

class AFlockManager;

UCLASS()
class FLOCKINGPROJECT_API AFlockActorInstance : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFlockActorInstance();

	void Update(float DeltaTime);
	void DetectEdge();
	FVector CheckObstacles();
	void Flock(TArray<AFlockActorInstance*> Neighbours);

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* Mesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FVector Position;
	FVector Velocity;
	FVector Acceleration;

	//Speed of the boid
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Flock Properties")
    bool AvoidObstacles;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Flock Properties")
	float MaxSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Flock Properties")
	float MaxForce;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Flock Properties")
	float MaxAvoidanceForce;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Flock Properties")
	float AccelerationMult;
	
	//Manager
	AFlockManager* FlockManager;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	void SetPosition(FVector inPosition);
	void UpdatePosition();

	FVector GetPosition();
	FVector GetCurrentVelocity();
	FVector GetAcceleration();

	void SetFlockManager(AFlockManager* Manager);

};
