// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FlockManager.generated.h"

class AFlockActorInstance;

UCLASS()
class FLOCKINGPROJECT_API AFlockManager : public AActor
{
	GENERATED_BODY()

	AFlockActorInstance* SpawnFlockInstance(FVector Location, FRotator Rotation);
	
	void ClearFlock();
	void UpdateFlock(float DeltaTime);

	TArray<AFlockActorInstance*> Instances;
	
public:	
	// Sets default values for this actor's properties
	AFlockManager();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Flock Properties")
	TSubclassOf<class AFlockActorInstance> FlockInstanceClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flock Properties")
	float RadiusOfInfluence;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flock Properties")
	FVector MinRegionSize;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flock Properties")
	FVector MaxRegionSize;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flock Properties")
	int BoidCount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flock Properties")
	float SeperationPower;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flock Properties")
	float CohesionPower;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flock Properties")
	float AlignmentPower;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category="Flock")
	void InitialiseFlock();
	
	float GetSeperationPower();
    float GetCohesionPower();
    float GetAlignmentPower();

};
